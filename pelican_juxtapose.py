# -*- coding: utf-8 -*-

# Copyright (c) 2016 Mike SHoup
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from docutils import nodes
from docutils.parsers.rst import directives, Directive

class Juxtapose(Directive):
    """ Embed Juxtapose comparisons in posts

    Create a Juxtapose slider at https://juxtapose.knightlab.com/
    Copy the unique ID generated when you click publish. This is your
    "SLIDER_ID", e.g., 58db2aac-cbd7-11e6-a768-0edaf8f81e27

    SLIDER_ID is required. The rest is optional.

    Usage:

    .. juxtapose:: SLIDER_ID
        :width: 640
        :height: 360
        :align: center
    """

    def align(argument):
        """Conversion function for the "align" option."""
        return directives.choice(argument, ('left', 'center', 'right'))

    required_arguments = 1
    optional_arguments = 3
    option_spec = {
        'width': directives.positive_int,
        'height': directives.positive_int,
        'align': align
    }

    final_argument_whitespace = False
    has_content = False

    def run(self):
        sliderID = self.arguments[0].strip()
        width = 640
        height = 360
        align = 'center'

        if 'width' in self.options:
            width = self.options['width']

        if 'height' in self.options:
            height = self.options['height']

        if 'align' in self.options:
            align = self.options['align']

        url = 'https://cdn.knightlab.com/libs/juxtapose/latest/embed/' +\
              'index.html?uid={}'.format(sliderID)
        div_block = '<div class="juxtapose" align="{}">'.format(align)
        embed_block = '<iframe width="{}" height="{}" src="{}" '\
                      'frameborder="0"></iframe>'.format(width, height, url)

        return [
            nodes.raw('', div_block, format='html'),
            nodes.raw('', embed_block, format='html'),
            nodes.raw('', '</div>', format='html')]


def register():
    	directives.register_directive('juxtapose', Juxtapose)
