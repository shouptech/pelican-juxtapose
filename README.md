# Pelican Juxtapose

A [Pelican](http://getpelican.com) plugin to make it easy to add
[Juxtapose](https://juxtapose.knightlab.com/) sliders to your blog.

## Usage

Create a Juxtapose slider at <https://juxtapose.knightlab.com/>
Copy the unique ID generated when you click publish. This is your
`SLIDER_ID`, e.g., `58db2aac-cbd7-11e6-a768-0edaf8f81e27`

`SLIDER_ID` is required. The rest is optional.

    .. juxtapose:: SLIDER_ID
        :width: 640
        :height: 360
        :align: center
